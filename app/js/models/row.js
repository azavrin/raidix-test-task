(function (BackboneModel) {
    var row = BackboneModel.extend({
        defaults: function () {
            return {
                name: '',
                date: new Date(),
                days: 0,
                mission: '',
                dateFormated: '',
                isMultiple: false,
                createdAt: new Date()
            };
        },
        //return date format dd/mm/yyyy from argument or model date
        dateFormating: function (value){
            var modelDate = value || this.get('date');
            var date = new Date(modelDate);
            return (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear(); 
        }
    });
    $.extend(true, App.Models, { Row: row });
}(Backbone.Model));
