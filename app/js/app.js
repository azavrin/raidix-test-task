﻿(function (Rows, AppView, Router, jsonexample) {
    app.collection = new Rows(jsonexample);
    
    app.appView = new AppView({
        collection: app.collection
    });

    app.router = new Router();

    Backbone.history.start();
})(App.Collections.Rows, App.Views.AppView, App.Router, jsonexample);