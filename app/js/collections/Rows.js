(function (BackboneCollection, Row) {
    var rows = BackboneCollection.extend({
        // Reference to this collection's model.
		model: Row,

		initialize: function (){
			var self = this;
			_.defer(function (){
				self.dateFormating();
			});
		},

		dateFormating: function () {
			_.each(this.models, function (model){
				model.set('dateFormated', model.dateFormating());
			});
		}
    });
    $.extend(true, App.Collections, { Rows: rows });
}(Backbone.Collection, App.Models.Row));
