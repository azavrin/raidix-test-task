(function (ItemView) {
    var rowView = ItemView.extend({
        tagName: "tr",
        template: _.template($('#row-template').html()),
        events: {
            "click .toggle": function () { this.toggleDone() },
            "dblclick td": function () { this.edit() },
            "click i.fa-check": function () { this.save() },
            "click i.fa-edit": function () { this.edit() },
            "click i.fa-times": function () { this.clear(); },
            "keypress .edit": function (e) { this.updateOnEnter(e); }
        },
        initialize: function () {
            this.listenTo(this.model, 'change', function () { this.render() });
            this.listenTo(this.model, 'destroy', function () { this.remove() });
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.inputName = this.$('.edit-name');
            this.inputDate = this.$('.edit-date');
            this.inputDays = this.$('.edit-days');
            this.inputMission = this.$('.edit-mission');
            this.inputIsmultiple = this.$('.edit-ismultiple');
            $(this.inputDate).datepicker();
            $(this.inputDate).datepicker({dateFormat: 'm/d/yy'}).datepicker("setDate", this.model.dateFormating());
            return this;
        },
        toggleDone: function () {
            this.model.toggle();
            this.model.save();
        },
        edit: function () {
            this.$el.addClass("editing");
            this.inputName.focus();
        },
        save: function (){
            this.model.set({ 
                name: this.inputName.val(),
                date: this.inputDate.datepicker('getDate'),
                dateFormated: this.model.dateFormating(this.inputDate.datepicker('getDate')),
                days: this.inputDays.val(),
                mission: this.inputMission.val(),
                isMultiple: this.inputIsmultiple.is(":checked")
            });
            this.close();
        },
        close: function () {
            this.$el.removeClass("editing");
        },
        updateOnEnter: function (e) {
            if (e.keyCode == 13) this.close();
        },
        clear: function () {
            this.model.destroy();
        }
    });
    $.extend(true, App.Views, { RowView: rowView });
})(Backbone.Marionette.ItemView);