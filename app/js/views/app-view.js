(function (CompositeView, RowView, Row) {
    var appView = CompositeView.extend({
        el: $("#app"),
        events: {
            "click #add-new-row": function () { this.create(); },
            "keypress #new-row": function (e) { if (e.keyCode == 13) { this.create(); } }
        },
        initialize: function () {
            this.inputName = this.$('#new-row .edit-name');
            this.inputDate = this.$('#new-row .edit-date');
            this.inputDays = this.$('#new-row .edit-days');
            this.inputMission = this.$('#new-row .edit-mission');
            this.inputIsmultiple = this.$('#new-row .edit-ismultiple');

            this.listenTo(this.collection, 'add', function (todo) { this.addOne(todo) });
            this.listenTo(this.collection, 'reset', function () { this.addAll() });
        },
        render: function () {
            this.addAll();
        },
        addOne: function (row) {
            var view = new RowView({ model: row });
            var el = view.render().el;
            this.$("#tbody").append(el);
        },
        addAll: function () {
            this.collection.each(this.addOne, this);
        },
        create: function () {
            if (!this.inputName.val() && !this.inputDate.val() 
                && !this.inputDays.val() 
                && !this.inputMission.val()) {
                return;
            }

            var model = new Row({
                name: this.inputName.val(),
                date: this.inputDate.datepicker('getDate'),
                days: this.inputDays.val(),
                mission: this.inputMission.val(),
                isMultiple: this.inputIsmultiple.is(":checked")
            });
            model.set('dateFormated', model.dateFormating(this.inputDate.datepicker('getDate')));
            this.collection.add(model);
            this.clearInputs();
        },
        clearInputs: function (){
            this.inputName.val('');
            this.inputDate.val('');
            this.inputDays.val('');
            this.inputMission.val('');
            this.inputIsmultiple.val('');
        }
    });
    $.extend(true, App.Views, { AppView: appView });
})(Backbone.Marionette.CompositeView, App.Views.RowView, App.Models.Row);