﻿(function (BackboneRouter) {
    router = BackboneRouter.extend({
        routes: {
            "": "app"
        },
        app: function () {
            app.appView.render();
        }
    });
    $.extend(true, App, { Router: router });
})(Backbone.Marionette.AppRouter);